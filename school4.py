x = input()

if x == 'треугольник':
    a = int(input())
    b = int(input())
    c = int(input())
    p = float((a + b + c) / 2)
    s = float(p * ((p - a) * (p - b) * (p - c))) ** 0.5
    print(s)
elif x == 'прямоугольник':
    a = int(input())
    b = int(input())
    print(a * b)
elif x == 'круг':
    r = int(input())
    p = 3.14
    print(p * (r ** 2))