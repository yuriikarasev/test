x = int(input())

a = x % 10
b = (x % 100) // 10
c = (x % 1000) // 100
d = (x % 10000) // 1000
e = (x % 100000) // 10000
f = (x % 1000000) // 100000

if (a + b + c) == (d + e + f):
    print('Счастливый')
else:
    print('Обычный')